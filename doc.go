// Copyright (c) 2021 Marc René Arns. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.

/*
Package linnstrument provides a library that helps developing applications for the LinnStrument (http://www.rogerlinndesign.com).

The LinnStrument comes in two flavors, LinnStrument200 and the LinnStrument128
with 200 and 128 playable led pads. This library supports both via the Device interface.

In addition to the playable led pads, both devices have a "control column" on the left where 8
led pads are located that won't play MIDI notes, but are used by the factory firmware
to allow to control its settings.

This library allows to set all available colors[1] and receive pressing and releasing events for both control pads
and playable pads. Whenever the row and column of a led pad is send or returned, the column 0 denotes the
control pads and the columns >= 1 the playable pads. The row however starts from top to bottom where top is
0 and bottom is 7.[3]

The LinnStrument allows to get pressing and releasing events for both control pads by switching to the "user
firmware mode"[3] where most of the factory firmware functionality is off. This mode allows the developer
to control the LinnStrument without interfering of the factory firmware. Only the "Global Setting" control
pad keeps its behavior, allowing the user to switch between user firmware mode and factory firmware mode while
pressing UPDATE OS for 0.5 sec. It is also possible to set the firmware mode by sending a MIDI message to the
device. To achieve this, UserFirmwareMode can be used. The user firmware mode is set implicitely when the StartListening
method is called and unset when the device is closed.

[1] See http://www.rogerlinndesign.com/ls-global-settings.html in section "Note Lights On/Off" for details about the
underlying technique.

[2] This is the reverse of the underlying MIDI where the rows are counted from bottom to top, but we found it
more convenient and helpful especially when using the library to trigger clips for Bitwig Studio and Reaper/Playtime.

[3] See https://github.com/rogerlinndesign/linnstrument-firmware/blob/master/user_firmware_mode.txt for details about firmware modes.

*/
package linnstrument
