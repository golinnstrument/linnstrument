package linnstrument

import "fmt"

type Error struct {
	X            uint8
	Y            uint8
	WrappedError error
	Task         string
}

func (e Error) Error() string {
	return fmt.Sprintf("LinnStrument had the following error when trying to set %d/%d in order to %s: %v", e.X, e.Y, e.Task, e.WrappedError)
}

type Errors struct {
	Task   string
	Errors []error
}

func (m *Errors) Len() int {
	return len(m.Errors)
}

func (m *Errors) Add(err error) {
	if err != nil {
		m.Errors = append(m.Errors, err)
	}
}

func (m *Errors) Error() string {
	return fmt.Sprintf("%d errors happened while setting, typecast to monome.Errors to inspect them", m.Len())
}
