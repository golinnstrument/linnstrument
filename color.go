package linnstrument

import (
	"io"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
)

var _ io.WriterTo = ColorRed.Pad(1, 1)

const (
	columnCC uint8 = 20
	rowCC    uint8 = 21
	colorCC  uint8 = 22
)

/*
From the Linnstrument manual (http://www.rogerlinndesign.com/ls-global-settings.html section "Note Lights On/Off")


	Setting the note lights remotely via MIDI messages

	You can set any of LinnStrument's note pad to any color by sending LinnStrument a series of three MIDI Control Change messages:

	1) CC20: Column number of note pad to change (control key column is 0, left play column is 1, right play column is 25)
	2) CC21: Row number of note pad to change (bottom row is 0, top is 7)
	3) CC22: Color to change it to (0=as set in Note Lights settings, 1=red, 2=yellow, 3=green, 4=cyan, 5=blue, 6=magenta, 7=off, 8=white,
	   9= orange, 10=lime and 11=pink).

	First send CC20 and CC21 to select the column and row to be lit, then send CC22 to light it.
*/

// Color is a possible color of a Linnstrument pad.
type Color uint8

const (
	ColorAsNoteLights Color = 0 // as set in Note Lights settings
	ColorRed          Color = 1
	ColorYellow       Color = 2
	ColorGreen        Color = 3
	ColorCyan         Color = 4
	ColorBlue         Color = 5
	ColorMagenta      Color = 6
	ColorOff          Color = 7
	ColorWhite        Color = 8
	ColorOrange       Color = 9
	ColorLime         Color = 10
	ColorPink         Color = 11
)

func (c Color) Value() uint8 {
	return uint8(c)
}

// Pad returns a ColorWriter for the pad at position row, column
func (c Color) Pad(row, column uint8) ColorWriter {
	return ColorWriter{Row: row, Column: column, Color: c}
}

// ColorWriter can be used to write the coloring to an io.Writer or to a midi.Writer
type ColorWriter struct {
	Row    uint8
	Column uint8
	Color  Color // 0-11, set color constants
}

// WriteTo writes the midi bytes to set the color of the pad to the given io.Writer.
func (p ColorWriter) WriteTo(wr io.Writer) (int64, error) {
	var bt []byte

	c := channel.Channel0
	bt = append(bt, c.ControlChange(columnCC, p.Column).Raw()...)
	bt = append(bt, c.ControlChange(rowCC, 7-p.Row).Raw()...)
	bt = append(bt, c.ControlChange(colorCC, uint8(p.Color)).Raw()...)

	n, err := wr.Write(bt)
	return int64(n), err
}

// Write writes the midi to set the color of the pad to the given midi.Writer
func (p ColorWriter) Write(wr midi.Writer) error {
	c := channel.Channel0
	err := wr.Write(c.ControlChange(columnCC, p.Column))
	if err != nil {
		return err
	}

	err = wr.Write(c.ControlChange(rowCC, 7-p.Row))
	if err != nil {
		return err
	}

	return wr.Write(c.ControlChange(colorCC, uint8(p.Color)))
}
