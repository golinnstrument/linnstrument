package linnstrument

import (
	"io"
	"sync"
	"time"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midi/reader"
	"gitlab.com/gomidi/midi/writer"
)

var (

	// RecommendedSleepDur is the recommended time to sleep between color setting calls to the LinnStrument
	RecommendedSleepDur = time.Microsecond * 150
)

// New128 returns a device that is appropriate for the LinnStrument128
// an connects it via the given MIDI in and out ports.
func New128(in midi.In, out midi.Out) Device {
	return newDevice("LinnStrument128", &cache128{}, in, out)
}

// New200 returns a device that is appropriate for the LinnStrument200
// an connects it via the given MIDI in and out ports.
func New200(in midi.In, out midi.Out) Device {
	return newDevice("LinnStrument200", &cache200{}, in, out)
}

// Device is a common interface for both the LinnStrument128 and the LinnStrument200.
type Device interface {

	// SetXData enables/disables sending of X-axis data for the given row
	SetXData(row uint8, enable bool) error

	// SetYData enables/disables sending of Y-axis data for the given row
	SetYData(row uint8, enable bool) error

	// SetZData enables/disables sending of Z-axis data for the given row
	SetZData(row uint8, enable bool) error

	// SetSlide enables/disables sending of slide data for the given row
	SetSlide(row uint8, enable bool) error

	// SetDataDecimation sets the data decimation
	SetDataDecimation(rateMS uint8) error

	// SetUserFirmwareMode sets the User Firmware mode (false: disable, true: enable)
	SetUserFirmwareMode(on bool) error

	// SetColor sets the pad at the given row and column to the given color.
	// Note that the first column (0) is the control pads on the left.
	// The rows are counted from top to bottom.
	SetColor(row, column uint8, color Color) error

	// SetColorAll sets all but the control pads to the given color. It has
	// reasonable sleeping time between the MIDI transmission to not
	// overload the LinnStrument.
	SetColorAll(color Color)

	// Cols returns the number of cols for the LinnStrument including
	// the control pads. Is 17 for the LinnStrument128 and 26 for the LinnStrument200.
	Cols() uint8

	// Rows returns the number of rows. Should always be 8.
	Rows() uint8

	// NumButtons returns the total number of buttons/pads, including the
	// control pads.
	NumButtons() uint8

	// String returns the name of the LinnStrument.
	String() string

	// StartListening starts the user firmware mode and listens on the pad presses of the LinnStrument
	// and calls the given function for each press and release of a pad.
	// If velocity is 0, the key was released, otherwise pressed with the given velocity.
	// Note that the rows count from top to bottom and the col starts with the control pads on the left.
	StartListening(Listener)

	// Close closes the connection of the LinnStrument and returns to the factory
	// firmware.
	Close() error

	// WhenOn sets the callback that is called each time when user mode is switched on
	// either manually via pressing Global Settings -> UPDATE OS for 30 sec. on the device
	// or via call to StartListening.
	// Restore is a function passed to callback, that when called will restore the pad color state
	// from the cache. The cache stores every color setting, even when the user mode is not active.
	// This way it is possible to seemlessly switch between user firmware mode and factory firmware
	// mode on the fly without loosing information that has been "set" in the meantime.
	//WhenOn(callback func(restore func()))

	// WhenOff sets the callback that is called each time when user mode is switched off
	// either manually via pressing Global Settings -> UPDATE OS for 30 sec. on the device
	// or via call to Close.
	//WhenOff(callback func())
}

type cache interface {
	setColor(x, y uint8, color Color)
	getColor(x, y uint8) (color Color)
	Cols() uint8
}

type device struct {
	wr   *writer.Writer
	rd   *reader.Reader
	in   midi.In
	out  midi.Out
	drv  midi.Driver
	isOn bool
	sync.RWMutex
	isClosed bool
	cache
	name    string
	whenOn  func(restore func())
	whenOff func()
}

func newDevice(name string, c cache, in midi.In, out midi.Out) *device {
	d := &device{}
	d.name = name
	d.cache = c
	d.wr = writer.New(out)
	d.in = in
	d.out = out
	d.prepareCache()
	return d
}

/*
func (d *device) WhenOn(callback func(restore func())) {
	d.Lock()
	d.whenOn = callback
	d.Unlock()
}

func (d *device) WhenOff(callback func()) {
	d.Lock()
	d.whenOff = callback
	d.Unlock()
}
*/

func (l *device) NumButtons() uint8 {
	return l.Rows() * l.Cols()
}

func (l *device) Rows() uint8 {
	return 8
}

func (l *device) String() string {
	return l.name
}

func (l *device) prepareCache() {
	for x := uint8(0); x < l.Rows(); x++ {
		for y := uint8(0); y < l.cache.Cols(); y++ {
			l.cache.setColor(x, y, ColorOff)
		}
	}
}

func (l *device) flushCache() {
	for x := uint8(0); x < l.Rows(); x++ {
		for y := uint8(0); y < l.cache.Cols(); y++ {
			color := l.cache.getColor(x, y)
			color.Pad(x, y).Write(l.wr)
		}
	}
}

// SetColorAll sets all but the controls to the given color
func (l *device) SetColorAll(color Color) {
	for x := uint8(0); x < l.Rows(); x++ {
		for y := uint8(1); y < l.cache.Cols(); y++ {
			l.SetColor(x, y, color)
			time.Sleep(RecommendedSleepDur)
		}
	}
}

func (l *device) SetColor(row, column uint8, color Color) (err error) {
	l.Lock()
	l.cache.setColor(row, column, color)
	if l.isOn {
		err = color.Pad(row, column).Write(l.wr)
	}
	/*
		 else {
			fmt.Println("setting suppressed (device is off)")
		}
	*/
	time.Sleep(RecommendedSleepDur)
	l.Unlock()
	return
}

func (l *device) Close() error {
	l.RLock()
	if l.isClosed {
		l.RUnlock()
		return nil
	}
	l.RUnlock()

	l.Lock()
	l.in.Close()
	l.isClosed = true
	l.Unlock()
	l.SetUserFirmwareMode(false)
	time.Sleep(RecommendedSleepDur)
	l.out.Close()
	return nil
}

func (l *device) userFirmwareModeOn() {
	l.Lock()
	wasOn := l.isOn
	l.isOn = true
	l.Unlock()
	if !wasOn && l.whenOn != nil {
		l.whenOn(l.flushCache)
	}
}

func (l *device) userFirmwareModeOff() {
	l.Lock()
	wasOn := l.isOn
	l.isOn = false
	l.Unlock()
	if wasOn && l.whenOff != nil {
		l.whenOff()
	}
}

// Listener listens for messages received from the LinnStrument
type Listener interface {

	// OnTouch is called, when a pad has been touched
	OnTouch(row, col, velocity uint8)

	// OnXDataMSB is called, when the MSB part of x-axis data was received (only when X-data was enabled on the device)
	OnXDataMSB(row, col uint8, globalMSB uint8)

	// OnXDataLSB is called, when the LSB part of x-axis data was received (only when X-data was enabled on the device)
	OnXDataLSB(row, col uint8, globalLSB uint8)

	// OnYData is called, when y-axis data was received (only when y-data was enabled on the device)
	OnYData(row, col, val uint8)

	// OnZData is called, when z-axis data was received (only when z-data was enabled on the device)
	OnZData(row, col, val uint8)

	// OnSlideTransition is called, when a slide transition message was received  (only when slide mode was enabled on the device)
	OnSlideTransition(row, col uint8)

	// OnUserFirmwareMode is called, when user firmware mode was enabled/disabled on the device
	// restoreColors function can be called to set the colors according to the last state that was set via SetColor calls.
	OnUserFirmwareMode(on bool, restoreColors func())
}

// if velocity is 0, the key was released, otherwise pressed
func (l *device) StartListening(li Listener) {

	var opts = []func(*reader.Reader){reader.NoLogger()}

	opts = append(opts, reader.PolyAftertouch(func(_ *reader.Position, channel, key, pressure uint8) {
		//	Polyphonic Pressure        Z data,       Note Number: Column,  Channel: Row,      Data: Per Cell Z Position
		li.OnZData(7-channel, key, pressure)
	}))

	opts = append(opts, reader.ControlChange(func(_ *reader.Position, channel, cc, val uint8) {
		switch {
		case cc == 119: // slide message
			// CC 119                     Cell Slide,                         Channel: Row,      Data: Transitioning Column
			li.OnSlideTransition(7-channel, val)
		case cc < 26: // X data Global X Position MSB
			// CC 0-25  X data,         CC Number: Column,  Channel: Row,      Data: Global X Position MSB
			li.OnXDataMSB(7-channel, cc, val)
		case cc > 31 && cc < 58: // Global X Position LSB
			// CC 32-57 X data,      CC Number-32: Column,  Channel: Row,      Data: Global X Position LSB
			li.OnXDataLSB(7-channel, cc-32, val)
		case cc > 63 && cc < 90: // Per Cell Y Position
			// CC 64-89 Y data,      CC Number-64: Column,  Channel: Row,      Data: Per Cell Y Position
			li.OnYData(7-channel, cc-64, val)
		}
	}))

	opts = append(opts, reader.NoteOn(func(_ *reader.Position, channel, key, velocity uint8) {
		l.RLock()
		isOn := l.isOn
		l.RUnlock()
		if isOn {
			if channel < l.Rows() && key < l.Cols() {
				li.OnTouch(7-channel, key, velocity)
			}
			//			fmt.Printf("got velocity: %v\n", velocity)
		}
	}))

	opts = append(opts, reader.NoteOff(func(_ *reader.Position, channel, key, velocity uint8) {
		l.RLock()
		isOn := l.isOn
		l.RUnlock()
		if isOn {
			if channel < l.Rows() && key < l.Cols() {
				li.OnTouch(7-channel, key, 0)
			}
		}
	}))

	l.rd = reader.New(opts...)

	var um UserFirmwareMode

	um.ScanReader(l.rd, func(userModeState bool) {
		l.Lock()
		l.isOn = userModeState
		l.Unlock()
		li.OnUserFirmwareMode(userModeState, l.flushCache)
	})

	go l.rd.ListenTo(l.in)
	l.SetUserFirmwareMode(true)
	time.Sleep(time.Millisecond * 400)
}

// UserFirmwareMode allows to set (true) or unset (false) the user firmware mode.
type UserFirmwareMode bool

// ScanReader provides a comfortable way to scan the given reader.Reader for firmware mode messages
// and calls the given callback, if any such message came in. If the user firmware mode is activated
// userFirmwareState is true, if the factory firmware mode is activated, userFirmwareState is false.
// Please keep in mind, that this overrides the Msg.Channel.ControlChange.NRPN.LSB handler.
// If you need to combine with your custom NRPN LSB handler, set it on the reader before passing
// the reader to this function. Then for any non firmware mode related NRPN LSB your custom handler will
// be called. The NRPN MSB handler however is not affected at all, since the MSB value is not significant for the firmware mode message.
func (f UserFirmwareMode) ScanReader(rd *reader.Reader, callback func(userFirmwareState bool)) {

	lsbHandler := func(pos *reader.Position, channel uint8, typ1, typ2, lsbVal uint8) {
		if typ1 == 1 && typ2 == 117 {
			switch lsbVal {
			case 1:
				callback(true)
			case 0:
				callback(false)
			}
		}
	}

	if rd.Channel.ControlChange.NRPN.LSB != nil {
		otherCallback := rd.Channel.ControlChange.NRPN.LSB
		lsbHandler = func(pos *reader.Position, channel uint8, typ1, typ2, lsbVal uint8) {
			if typ1 == 1 && typ2 == 117 {
				switch lsbVal {
				case 1:
					callback(true)
				case 0:
					callback(false)
				}
			} else {
				otherCallback(pos, channel, typ1, typ2, lsbVal)
			}
		}
	}

	//rd.Callback(reader.NrpnLSB(lsbHandler))

	rd.Channel.ControlChange.NRPN.LSB = lsbHandler
}

// Write writes the MIDI messages to set (true) or unset (false) the user firmware mode to the given midi.Writer.
func (f UserFirmwareMode) Write(wr midi.Writer) error {
	msgs := f.messages()

	for _, msg := range msgs {
		err := wr.Write(msg)
		if err != nil {
			return err
		}
		time.Sleep(RecommendedSleepDur)
	}
	return nil
}

var _ io.WriterTo = UserFirmwareMode(true)

// Write writes the MIDI messages to set (true) or unset (false) the user firmware mode to the given io.Writer.
func (f UserFirmwareMode) WriteTo(wr io.Writer) (int64, error) {
	var bt []byte

	msgs := f.messages()

	for _, msg := range msgs {
		bt = append(bt, msg.Raw()...)
	}

	n, err := wr.Write(bt)
	return int64(n), err
}

func (_ UserFirmwareMode) cc(num, val uint8) midi.Message {
	return channel.Channel0.ControlChange(num, val)
}

func (f UserFirmwareMode) messages() []midi.Message {
	var val uint8
	if f {
		val = 1
	}
	return []midi.Message{
		// set the NRPN
		f.cc(99, 1),
		f.cc(98, 117),
		f.cc(6, 0),
		f.cc(38, val),

		// not strictly needed, but good style: reset RPN
		/*
			f.cc(101, 127),
			f.cc(100, 127),
		*/
	}
}

type Row uint8 // Row starts counting from 0 (top) to 7 (bottom)

// XData returns a midi message that enableds/disables the sending of the X-axis data for the row
func (r Row) XData(enable bool) midi.Message {
	var val uint8
	if enable {
		val = 1
	}
	return channel.Channel(uint8(7-r)).ControlChange(10, val)
}

// YData returns a midi message that enableds/disables the sending of the X-axis data for the row
func (r Row) YData(enable bool) midi.Message {
	var val uint8
	if enable {
		val = 1
	}
	return channel.Channel(uint8(7-r)).ControlChange(11, val)
}

// ZData returns a midi message that enableds/disables the sending of the X-axis data for the row
func (r Row) ZData(enable bool) midi.Message {
	var val uint8
	if enable {
		val = 1
	}
	return channel.Channel(uint8(7-r)).ControlChange(12, val)
}

func (r Row) Slide(enable bool) midi.Message {
	var val uint8
	if enable {
		val = 1
	}
	return channel.Channel(uint8(7-r)).ControlChange(9, val)
}

// the value corresponds to the number of milliseconds between data updates (minimum 12 ms in low power mode)
func DataDecimation(rateMS uint8) midi.Message {
	return channel.Channel0.ControlChange(13, rateMS)
}

// SetXData enables/disables sending of X-axis data for the given row
func (l *device) SetXData(row uint8, enable bool) error {
	return l.wr.Write(Row(row).XData(enable))
}

// SetYData enables/disables sending of Y-axis data for the given row
func (l *device) SetYData(row uint8, enable bool) error {
	return l.wr.Write(Row(row).YData(enable))
}

// SetZData enables/disables sending of Z-axis data for the given row
func (l *device) SetZData(row uint8, enable bool) error {
	return l.wr.Write(Row(row).ZData(enable))
}

// SetSlide enables/disables sending of slide data for the given row
func (l *device) SetSlide(row uint8, enable bool) error {
	return l.wr.Write(Row(row).Slide(enable))
}

// SetDataDecimation sets the data decimation
func (l *device) SetDataDecimation(rateMS uint8) error {
	return l.wr.Write(DataDecimation(rateMS))
}

//  SetUserFirmwareMode sets the User Firmware mode (false: disable, true: enable)
func (l *device) SetUserFirmwareMode(on bool) error {
	return UserFirmwareMode(on).Write(l.wr)
}
