package linnstrument

type cache128 struct {
	cache [8][17]Color
}

func (l *cache128) setColor(row, col uint8, color Color) {
	l.cache[row][col] = color
}

func (l *cache128) getColor(row, col uint8) (color Color) {
	return l.cache[row][col]
}

func (l *cache128) Cols() uint8 {
	return 17
}

type cache200 struct {
	cache [8][26]Color
}

func (l *cache200) setColor(row, col uint8, color Color) {
	l.cache[row][col] = color
}

func (l *cache200) getColor(row, col uint8) (color Color) {
	return l.cache[row][col]
}

func (l *cache200) Cols() uint8 {
	return 26
}
