package linnstrument

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/gomidi/midi"
)

func find(drv midi.Driver) (in midi.In, out midi.Out, err error) {
	defer func() {
		if err != nil {
			drv.Close()
		}
	}()

	ins, err := drv.Ins()

	if err != nil {
		return nil, nil, err
	}

	var inPort = -1

	for _, in := range ins {
		if strings.Contains(in.String(), "LinnStrument") {
			inPort = in.Number()
			break
		}
	}

	if inPort < 0 {
		return nil, nil, fmt.Errorf("can't find LinnStrument in port, maybe not connected via USB?")
	}

	in, err = midi.OpenIn(drv, inPort, "")

	if err != nil {
		return nil, nil, err
	}

	outs, err := drv.Outs()

	if err != nil {
		return nil, nil, err
	}

	var outPort = -1

	for _, out := range outs {
		if strings.Contains(out.String(), "LinnStrument") {
			outPort = out.Number()
			break
		}
	}

	if outPort < 0 {
		return nil, nil, fmt.Errorf("can't find LinnStrument out port, maybe not connected via USB?")
	}

	out, err = midi.OpenOut(drv, outPort, "")

	if err != nil {
		return nil, nil, err
	}

	return in, out, nil
}

// Find128 tries to find a LinnStrument on a MIDI port by looking for the string "LinnStrument"
// in the port name which would be only there, if the LinnStrument is connected via USB.
// The first port that is found is opened and a device is returned that would be appropriate
// for a LinnStrument128
func Find128(drv midi.Driver) (Device, error) {
	in, out, err := find(drv)
	if err != nil {
		return nil, err
	}

	return New128(in, out), nil
}

// Find200 tries to find a LinnStrument on a MIDI port by looking for the string "LinnStrument"
// in the port name which would be only there, if the LinnStrument is connected via USB.
// The first port that is found is opened and a device is returned that would be appropriate
// for a LinnStrument200
func Find200(drv midi.Driver) (Device, error) {
	in, out, err := find(drv)
	if err != nil {
		return nil, err
	}

	return New200(in, out), nil
}

func Greeter(d Device) error {
	return Marquee(d, d.String(), time.Millisecond*170, ColorYellow, ColorBlue)
}

// Print prints the given string to the device. If the string is too long, anything too long is not printed
func Print(m Device, s string, textcolor, bgcolor Color) (err error) {
	var errs Errors
	s = strings.ToLower(s)
	m.SetColorAll(bgcolor)

	// cols is the linear row of all letters, where each letter has some cols
	//var cols = make([][8]bool, len(s)*8)
	var cols [][8]bool

	for _, l := range s {
		lt, has := _Letters[l]
		if !has {
			continue
		}

		letter := make([][8]bool, letterWidth[l])

		for pt, v := range lt {
			if v {
				letter[pt[1]][pt[0]] = true
			}
		}

		for _, col := range letter {
			cols = append(cols, col)
		}
	}

	// i is the starting point
	width := int(m.Cols() - 1)

	var targetCol uint8 = 0
	for j := 0; j < (width) && j < len(cols); j++ {

		for row, on := range cols[j] {
			//err = m.Switch(uint8(row), targetCol, on)
			if on {
				dist := width/2 - int(targetCol)
				if dist < 0 {
					dist *= (-1)
				}
				//err = m.Set(uint8(row), targetCol, uint8(15-dist))
				err = m.SetColor(uint8(row), targetCol+1, textcolor)
			} else {
				err = m.SetColor(uint8(row), targetCol+1, bgcolor)
			}
			if err != nil {
				e := err.(Error)
				what := "off"
				if on {
					what = "on"
				}
				e.Task = fmt.Sprintf("switch %s %d/%d on LinnStrument while marqueing", what, uint8(row), targetCol+1)
				errs.Add(e)
				return &errs
			}
		}
		targetCol++
	}

	return nil
}

// Marquee displays the given string in a marquee-like manner on the device (from right to left).
func Marquee(m Device, s string, dur time.Duration, textcolor, bgcolor Color) (err error) {
	var errs Errors
	s = strings.ToLower(s)
	s = "   " + s + " "
	m.SetColorAll(bgcolor)

	// cols is the linear row of all letters, where each letter has some cols
	//var cols = make([][8]bool, len(s)*8)
	var cols [][8]bool

	for _, l := range s {
		lt, has := _Letters[l]
		if !has {
			continue
		}

		letter := make([][8]bool, letterWidth[l])

		for pt, v := range lt {
			if v {
				letter[pt[1]][pt[0]] = true
			}
		}

		for _, col := range letter {
			cols = append(cols, col)
		}
	}

	// i is the starting point
	width := int(m.Cols() - 1)
	for i := 0; i < len(cols); i++ {
		var targetCol uint8 = 0
		for j := i; j < (i+width) && j < len(cols); j++ {

			for row, on := range cols[j] {
				//err = m.Switch(uint8(row), targetCol, on)
				if on {
					dist := width/2 - int(targetCol)
					if dist < 0 {
						dist *= (-1)
					}
					//err = m.Set(uint8(row), targetCol, uint8(15-dist))
					err = m.SetColor(uint8(row), targetCol+1, textcolor)
				} else {
					err = m.SetColor(uint8(row), targetCol+1, bgcolor)
				}
				if err != nil {
					e := err.(Error)
					what := "off"
					if on {
						what = "on"
					}
					e.Task = fmt.Sprintf("switch %s %d/%d on LinnStrument while marqueing", what, uint8(row), targetCol+1)
					errs.Add(e)
					return &errs
				}
			}
			targetCol++
		}
		//time.Sleep(time.Millisecond * 60)
		time.Sleep(dur)
	}

	return nil
}
