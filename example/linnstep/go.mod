module gitlab.com/golinnstrument/linnstrument/example/linnstep

go 1.14

require (
	gitlab.com/golinnstrument/linnstrument v0.0.0
	gitlab.com/gomidi/midi v1.23.7
	gitlab.com/gomidi/midicatdrv v0.3.7
	gitlab.com/metakeule/config v1.21.0
)

replace (
	gitlab.com/golinnstrument/linnstrument => ../../
)
