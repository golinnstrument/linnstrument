package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midi/reader"
	"gitlab.com/gomidi/midi/writer"

	// if you don't want to use the binary midicat, you can use the rtmididrv instead
	// driver "gitlab.com/gomidi/rtmididrv"
	driver "gitlab.com/gomidi/midicatdrv"
	"gitlab.com/metakeule/config"
)

var (
	cfg                = config.MustNew("linnswitch", "0.0.1", "example to play MIDI notes as switches on the linnstrument")
	outArg             = cfg.NewInt32("out", "number of the MIDI output port to send the MIDI notes triggered by the linnstrument to", config.Required, config.Shortflag('o'))
	inArg              = cfg.NewInt32("in", "number of the MIDI input port to receive the feedback", config.Shortflag('i'))
	linoutArg          = cfg.NewInt32("linout", "number of the MIDI output port for the linnstrument")
	lininArg           = cfg.NewInt32("linin", "number of the MIDI input port for the linnstrument")
	feedbackReverseArg = cfg.NewBool("feedbackreverse", "reverse note on / off switch behavior for feedback")
	outChannelArg      = cfg.NewInt32("chan", "output MIDI channel (0-15)", config.Default(int32(0)), config.Shortflag('c'))
	switchArg          = cfg.NewBool("switch", "let the linnstrument act like a switch", config.Default(false), config.Shortflag('s'))
	listCmd            = cfg.MustCommand("ports", "list MIDI in and out ports").SkipAllBut()
	sigchan            = make(chan os.Signal, 10)
)

func main() {
	// this is to check, if the midicat binary is in your PATH. Uncomment, if you want to use another driver
	err := driver.CheckMIDICatBinary(os.Stdout)

	if err != nil {
		os.Exit(1)
	}

	err = run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {

	err := cfg.Run()

	if err != nil {
		fmt.Fprintln(os.Stdout, cfg.Usage())
		return err
	}

	drv, err := driver.New()

	if err != nil {
		return err
	}

	defer drv.Close()

	if cfg.ActiveCommand() == listCmd {
		return printMIDIPorts(drv)
	}

	var linstr linnstrument.Device

	var ch = uint8(outChannelArg.Get())

	if ch < 0 || ch > 15 {
		return fmt.Errorf("invalid MIDI channel must be 0-15")
	}

	out, err := midi.OpenOut(drv, int(outArg.Get()), "")
	if err != nil {
		return err
	}

	if linoutArg.IsSet() && lininArg.IsSet() {

		linin, err := midi.OpenIn(drv, int(lininArg.Get()), "")
		if err != nil {
			return err
		}

		linout, err := midi.OpenOut(drv, int(linoutArg.Get()), "")
		if err != nil {
			return err
		}

		linstr = linnstrument.New128(linin, linout)
	} else {

		linstr, err = linnstrument.Find128(drv)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not find LinnStrument by name. Maybe not connected via USB?\nTry to pass the port numbers via --in and --out parameters.\n")
		return printMIDIPorts(drv)
	}

	wr := writer.New(out)
	wr.SetChannel(ch)

	list := &Listener{linstr: linstr, wr: wr}

	if inArg.IsSet() {
		in, err := midi.OpenIn(drv, int(inArg.Get()), "")
		if err != nil {
			return err
		}

		rd := reader.New(
			reader.NoLogger(),
			reader.Each(func(_ *reader.Position, msg midi.Message) {
				if feedbackReverseArg.Get() {
					switch v := msg.(type) {
					case channel.NoteOn:
						if v.Velocity() == 0 {
							list.switchOn(v.Key())
						} else {
							list.switchOff(v.Key())
						}
					case channel.NoteOff:
						list.switchOn(v.Key())
					case channel.NoteOffVelocity:
						list.switchOn(v.Key())
					}
				} else {
					switch v := msg.(type) {
					case channel.NoteOn:
						if v.Velocity() == 0 {
							list.switchOff(v.Key())
						} else {
							list.switchOn(v.Key())
						}
					case channel.NoteOff:
						list.switchOff(v.Key())
					case channel.NoteOffVelocity:
						list.switchOff(v.Key())
					}
				}
			}))

		rd.ListenTo(in)
	}

	linstr.StartListening(list)

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted, cleaning up...\n")
	linstr.Close()
	time.Sleep(linnstrument.RecommendedSleepDur)

	return nil
}

func printMIDIPorts(drv midi.Driver) error {
	outs, err := drv.Outs()

	if err != nil {
		return err
	}

	fmt.Fprintln(os.Stdout, "\nMIDI outputs\n")

	for _, out := range outs {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", out.Number(), out.String())
	}

	ins, err := drv.Outs()

	if err != nil {
		return err
	}

	fmt.Fprintln(os.Stdout, "\nMIDI inputs\n")

	for _, in := range ins {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", in.Number(), in.String())
	}
	return nil
}

type Listener struct {
	linstr      linnstrument.Device
	infoPrinted bool
	isSet       [128]bool
	wr          *writer.Writer
	mx          sync.RWMutex
}

func (l *Listener) print(text string) {
	linnstrument.Print(l.linstr, text, linnstrument.ColorGreen, linnstrument.ColorOff)
}

func (l *Listener) marquee(text string) {
	linnstrument.Marquee(l.linstr, text, time.Millisecond*8, linnstrument.ColorGreen, linnstrument.ColorOff)
}

func (l *Listener) OnUserFirmwareMode(on bool, restore func()) {
	if on && !l.infoPrinted {
		fmt.Fprintln(os.Stdout, "listening for pad pressing. CTRL+C to quit")
		l.print("Go!")
		time.Sleep(time.Millisecond * 380)
		l.linstr.SetColorAll(linnstrument.ColorOff)
		l.infoPrinted = true
	}
}

func (l *Listener) OnSlideTransition(uint8, uint8) {

}

func (l *Listener) OnXDataLSB(uint8, uint8, uint8) {

}

func (l *Listener) OnYData(uint8, uint8, uint8) {

}

func (l *Listener) OnZData(uint8, uint8, uint8) {

}

func (l *Listener) OnXDataMSB(uint8, uint8, uint8) {

}

func (l *Listener) noteOn(x, y, key, velocity uint8) {
	writer.NoteOn(l.wr, key, velocity)
	//	l.linstr.SetColor(x, y, linnstrument.ColorBlue)
}

func (l *Listener) noteOff(x, y, key uint8) {
	writer.NoteOff(l.wr, key)
	//	l.linstr.SetColor(x, y, linnstrument.ColorOff)
}

func (l *Listener) xyFromKey(key uint8) (x, y uint8) {
	//key := x*16 + y - 1
	// TODO: test
	y = (key % 16) + 1
	x = key / 16
	return
}

func (l *Listener) switchOn(key uint8) {
	fmt.Printf("switch on key: %v\n", key)
	l.mx.Lock()
	l.isSet[key] = true
	l.mx.Unlock()
	x, y := l.xyFromKey(key)
	l.linstr.SetColor(x, y, linnstrument.ColorBlue)
}

func (l *Listener) switchOff(key uint8) {
	fmt.Printf("switch off key: %v\n", key)
	l.mx.Lock()
	l.isSet[key] = false
	l.mx.Unlock()
	x, y := l.xyFromKey(key)
	l.linstr.SetColor(x, y, linnstrument.ColorOff)
}

func (l *Listener) switchNote(x, y, key, velocity uint8) {
	if velocity == 0 {
		return
	}
	l.mx.Lock()
	isset := l.isSet[key]
	defer l.mx.Unlock()

	if isset {
		l.isSet[key] = false
		l.noteOff(x, y, key)
		l.linstr.SetColor(x, y, linnstrument.ColorOff)
		return
	}

	l.isSet[key] = true
	l.noteOn(x, y, key, velocity)
	l.linstr.SetColor(x, y, linnstrument.ColorBlue)
}

func (l *Listener) OnTouch(x, y, velocity uint8) {
	key := x*16 + y - 1

	if switchArg.Get() {
		l.switchNote(x, y, key, velocity)
		return
	}

	if velocity > 0 { // key is pressed
		l.noteOn(x, y, key, velocity)

		l.mx.RLock()
		isset := l.isSet[key]
		l.mx.RUnlock()

		if isset {
			l.switchOff(key)
		} else {
			l.switchOn(key)
		}
		return
	}

	l.noteOff(x, y, key)
}
