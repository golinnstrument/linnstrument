package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/writer"

	// if you don't want to use the binary midicat, you can use the rtmididrv instead
	// driver "gitlab.com/gomidi/rtmididrv"
	driver "gitlab.com/gomidi/midicatdrv"
	"gitlab.com/metakeule/config"
)

var (
	cfg         = config.MustNew("linnusermode", "0.0.1", "example to set the set the firmware/user mode for the linnstrument")
	userModeArg = cfg.LastBool("switch", "sets (true/t) or unsets (false/f) the user firmware mode", config.Required)
	outArg      = cfg.NewInt32("out", "number of the MIDI output port for the linnstrument", config.Shortflag('o'))
	listCmd     = cfg.MustCommand("ports", "available MIDI out ports").Relax("out").Relax("switch")
	sigchan     = make(chan os.Signal, 10)
)

func main() {
	// this is to check, if the midicat binary is in your PATH. Uncomment, if you want to use another driver
	err := driver.CheckMIDICatBinary(os.Stdout)

	if err != nil {
		os.Exit(1)
	}

	err = run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func run() (err error) {
	err = cfg.Run()

	if err != nil {
		fmt.Fprintln(os.Stdout, cfg.Usage())
		return
	}

	drv, err := driver.New()

	if err != nil {
		return err
	}

	defer drv.Close()

	if cfg.ActiveCommand() == listCmd {
		return printMIDIPorts(drv)
	}

	var out midi.Out

	if outArg.IsSet() {
		out, err = midi.OpenOut(drv, int(outArg.Get()), "")
	} else {
		out, err = midi.OpenOut(drv, -1, "LinnStrument")
	}

	if err != nil {
		return err
	}

	wr := writer.New(out)
	err = linnstrument.UserFirmwareMode(userModeArg.Get()).Write(wr)

	if err != nil {
		return err
	}

	time.Sleep(time.Second)

	return nil
}

func printMIDIPorts(drv midi.Driver) error {
	outs, err := drv.Outs()

	if err != nil {
		return err
	}

	fmt.Fprintln(os.Stdout, "MIDI outputs")

	for _, out := range outs {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", out.Number(), out.String())
	}

	return nil
}
