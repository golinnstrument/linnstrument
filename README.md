# linnstrument

This is a library for programming the LinnStrument (https://www.rogerlinndesign.com/linnstrument).

Based on the `user firmware mode`of the LinnStrument (see https://github.com/rogerlinndesign/linnstrument-firmware/blob/master/user_firmware_mode.txt)
this library helps to create programs with Go that get the pad presses and set the lights.

## Status

Stable and usable with firmware version 2.2.1 and later.

## Features

Practically all that is possible with the user firmware mode:

- switch user firmware mode on and off
- get pad presses
- set colors of pads

Additionally a little gimmick:

- show colored text on the LinnStrument


## Installation

```
go get -d -u gilab.com/golinnstrument/linnstrument
```

## Documentation (library)

https://pkg.go.dev/gitlab.com/golinnstrument/linnstrument

## Examples 

See https://gitlab.com/golinnstrument/linnstrument/-/tree/master/example.
